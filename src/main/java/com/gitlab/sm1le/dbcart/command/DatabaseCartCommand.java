package com.gitlab.sm1le.dbcart.command;

import com.gitlab.sm1le.dbcart.DatabaseCartPlugin;
import com.gitlab.sm1le.dbcart.api.CartUserManager;
import com.gitlab.sm1le.util.Messenger;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import java.util.List;

public class DatabaseCartCommand implements CommandExecutor {

    private CartUserManager manager;
    private Messenger messenger;
    private DatabaseCartPlugin plugin;

    public DatabaseCartCommand(DatabaseCartPlugin plugin) {
        this.manager = plugin.getManager();
        this.messenger = plugin.getMessenger();
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(args.length < 1) {
            commandSender.sendMessage(messenger.getMessage("invalid-syntax"));
            return true;
        }
        if(args[0].equalsIgnoreCase("reconnect")) {
            if(!commandSender.hasPermission("dbcart.reconnect")) {
                commandSender.sendMessage(messenger.getMessage("no-permission"));
                return true;
            }
            plugin.reconnect();
            commandSender.sendMessage(messenger.getMessage("successfully-reconnected"));
            return true;
        }
        if(args[0].equalsIgnoreCase("reload")) {
            if(!commandSender.hasPermission("dbcart.reload")) {
                commandSender.sendMessage(messenger.getMessage("no-permission"));
                return true;
            }
            plugin.reloadConfig();
            commandSender.sendMessage(messenger.getMessage("config-successfully-reloaded"));
            return true;
        }
        if(args[0].equalsIgnoreCase("get")) {
            if(args.length < 2) {
                commandSender.sendMessage(messenger.getMessage("invalid-syntax"));
                return true;
            }
            List<String> commands = manager.getUser(commandSender.getName()).getPurchasedCommands(args[1]);
            if(commands.size() == 0) {
                commandSender.sendMessage(messenger.getMessage("commands-not-found"));
                return true;
            }
            for (String cmd : commands) {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd.replace("{player}", commandSender.getName()));
            }
            manager.getUser(commandSender.getName()).reset(args[1]);
            commandSender.sendMessage(messenger.getMessage("commands-successfully-executed"));
            return true;
        }
        return false;
    }

}
