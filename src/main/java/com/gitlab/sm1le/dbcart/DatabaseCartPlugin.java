package com.gitlab.sm1le.dbcart;

import com.gitlab.sm1le.dbcart.api.CartUserManager;
import com.gitlab.sm1le.dbcart.command.DatabaseCartCommand;
import com.gitlab.sm1le.query.QueryBuilder;
import com.gitlab.sm1le.sql.connection.AbstractConnection;
import com.gitlab.sm1le.sql.connection.MySQLConnection;
import com.gitlab.sm1le.util.Messenger;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.Connection;
import java.sql.SQLException;

public class DatabaseCartPlugin extends JavaPlugin {

    private AbstractConnection mainConnection;
    private CartUserManager manager;
    private Messenger messenger;

    @Override
    public void onEnable() {
        this.saveDefaultConfig();
        this.mainConnection = new MySQLConnection(getConfig().getString("settings.mysql.host"), getConfig().getString("settings.mysql.database"), getConfig().getString("settings.mysql.user"), getConfig().getString("settings.mysql.password"));
        try(Connection connection = mainConnection.getConnection()) {
            QueryBuilder.of(connection, getConfig().getString("settings.mysql.table")).createTableQuery()
                    .ifNotExists()
                    .column("PLAYER", "VARCHAR")
                    .column("SERVER", "VARCHAR")
                    .column("TYPE", "VARCHAR")
                    .column("COMMAND", "VARCHAR")
                    .execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            this.manager = new CartUserManager(mainConnection.getConnection(), getConfig());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        this.messenger = new Messenger(getConfig().getConfigurationSection("messages"));
        getCommand("databasecart").setExecutor(new DatabaseCartCommand(this));
    }

    @Override
    public void onDisable() {
        manager.close();
        mainConnection.close();
    }

    public Messenger getMessenger() {
        return messenger;
    }

    public CartUserManager getManager() {
        return manager;
    }

    public void reconnect() {
        this.mainConnection = new MySQLConnection(getConfig().getString("settings.mysql.host"), getConfig().getString("settings.mysql.database"), getConfig().getString("settings.mysql.user"), getConfig().getString("settings.mysql.password"));
    }
}
