package com.gitlab.sm1le.dbcart.api;

import org.bukkit.configuration.file.FileConfiguration;

import java.sql.Connection;
import java.sql.SQLException;

public class CartUserManager {

    private Connection connection;
    private FileConfiguration mainConfig;

    public CartUserManager(Connection connection, FileConfiguration mainConfig) {
        this.connection = connection;
        this.mainConfig = mainConfig;
    }

    public CartUser getUser(String name) {
        return new CartUser(name, connection, mainConfig);
    }

    public void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
