package com.gitlab.sm1le.dbcart.api;

import com.gitlab.sm1le.query.QueryBuilder;
import org.bukkit.configuration.file.FileConfiguration;

import java.sql.Connection;
import java.util.List;
import java.util.stream.Collectors;

public class CartUser {

    private String name;
    private Connection connection;
    private FileConfiguration mainConfig;

    CartUser(String name, Connection connection, FileConfiguration mainConfig) {
        this.name = name;
        this.connection = connection;
        this.mainConfig = mainConfig;
    }

    public String getName() {
        return name;
    }

    public List<String> getPurchasedCommands(String type) {
        return QueryBuilder.of(connection, mainConfig.getString("settings.mysql.table")).selectQuery()
                .where("SERVER", mainConfig.getString("settings.server"))
                .where("TYPE", type, "ALL")
                .where("PLAYER", name)
                .returns("COMMAND")
                .execute().getRows().stream()
                .map(row -> row.getValue("COMMAND", String.class)).collect(Collectors.toList());
    }

    public void reset(String type) {
        QueryBuilder.of(connection, mainConfig.getString("settings.mysql.table")).deleteQuery()
                .where("SERVER", mainConfig.getString("settings.server"))
                .where("TYPE", type, "ALL")
                .where("PLAYER", name)
                .execute();
    }

}
